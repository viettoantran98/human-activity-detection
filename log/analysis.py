import math
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn import svm
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

filename = "2.txt"
with open(filename) as f:
    content = f.readlines()

walking = [[], []]
standing = [[], []]
sitting = [[], []]
count = 1
for line in content:
    if line in ('\n', '\r\n'):
        count += 1
        continue
    if count % 3 == 1:
        walking[int(count / 3)].append(line)
    elif count % 3 == 2:
        standing[int(count / 3)].append(line)
    else:
        sitting[int(count / 3) - 1].append(line)

BATCH_SIZE = 150
walking_split = []
standing_split = []
sitting_split = []

for idx in range(2):
    num_batch = int(len(walking[idx]) / BATCH_SIZE)
    for i in range(num_batch):
        walking_split.append(walking[idx][i*BATCH_SIZE : (i+1)*BATCH_SIZE - 1])
for idx in range(2):
    num_batch = int(len(standing[idx]) / BATCH_SIZE)
    for i in range(num_batch):
        standing_split.append(standing[idx][i*BATCH_SIZE : (i+1)*BATCH_SIZE - 1])
for idx in range(2):
    num_batch = int(len(sitting[idx]) / BATCH_SIZE)
    for i in range(num_batch):
        sitting_split.append(sitting[idx][i*BATCH_SIZE : (i+1)*BATCH_SIZE - 1])

walking_AI_SMA = []
standing_AI_SMA = []
sitting_AI_SMA = []

for walking_batch in walking_split:
    AI = 0.0
    SMA = 0.0
    for j, item in enumerate(walking_batch):
        data = [float(x) for x in item.split()]
        AI += math.sqrt(data[3]*data[3] + data[4]*data[4] + data[5]*data[5])
        SMA += abs(data[3]) + abs(data[4]) + abs(data[5])
    AI = AI / BATCH_SIZE
    SMA = SMA / BATCH_SIZE
    walking_AI_SMA.append([AI, SMA])

for standing_batch in standing_split:
    AI = 0.0
    SMA = 0.0
    for j, item in enumerate(standing_batch):
        data = [float(x) for x in item.split()]
        AI += math.sqrt(data[3]*data[3] + data[4]*data[4] + data[5]*data[5])
        SMA += abs(data[3]) + abs(data[4]) + abs(data[5])
    AI = AI / BATCH_SIZE
    SMA = SMA / BATCH_SIZE
    standing_AI_SMA.append([AI, SMA])

for sitting_batch in sitting_split:
    AI = 0.0
    SMA = 0.0
    for j, item in enumerate(sitting_batch):
        data = [float(x) for x in item.split()]
        AI += math.sqrt(data[3]*data[3] + data[4]*data[4] + data[5]*data[5])
        SMA += abs(data[3]) + abs(data[4]) + abs(data[5])
    AI = AI / BATCH_SIZE
    SMA = SMA / BATCH_SIZE
    sitting_AI_SMA.append([AI, SMA])

walking_AI = [x[0] for x in walking_AI_SMA]
walking_SMA = [x[1] for x in walking_AI_SMA]
plt.plot(walking_AI,walking_SMA,'ro')

standing_AI = [x[0] for x in standing_AI_SMA]
standing_SMA = [x[1] for x in standing_AI_SMA]
plt.plot(standing_AI,standing_SMA,'bo')

sitting_AI = [x[0] for x in sitting_AI_SMA]
sitting_SMA = [x[1] for x in sitting_AI_SMA]



X = walking_AI_SMA + standing_AI_SMA + sitting_AI_SMA
y = [0] * len(walking_AI_SMA) + [1] * len(standing_AI_SMA) + [2] * len(sitting_AI_SMA)
# kmeans = KMeans(n_clusters=3, random_state=0).fit(X)
# print(np.count_nonzero(Y==kmeans.labels_) / len(Y))

# print(kmeans.labels_)


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

clf = svm.SVC(gamma='scale')
clf.fit(X_train, y_train)

y_pred = clf.predict(X_test)

print("Accuracy: %.2f %%" %(100*accuracy_score(y_test, y_pred)))


plt.plot(sitting_AI,sitting_SMA,'go')
plt.xlabel('AI')
plt.ylabel('SMA')
plt.show()