# all: main.o SFE_LSM9DS0.o
# 	g++ -lmraa main.o SFE_LSM9DS0.o -o output
# main.o: main.cpp
# 	g++ -c main.cpp
# SFE_LSM9DS0.o: SFE_LSM9DS0.cpp SFE_LSM9DS0.h
# 	g++ -c SFE_LSM9DS0.cpp

# clean:
# 	rm *.o output
CC=g++ -lmraa
CFLAGS=-c -Wall
LDFLAGS=
SOURCES=spi/spi_port_edison.cpp spi/spi_device_edison.cpp main.cpp oled/Edison_OLED.cpp gpio/gpio_edison.cpp SFE_LSM9DS0.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=output

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf *.o $(EXECUTABLE)